﻿using System;

namespace exercise_25
{
    class Person
    {
        string Name;  //Task A - Create a Class with 2 fields (private)
        int Age;
        public Person(string _name, int _age) //Task A - Create a constructor (Public)
        {
            Name = _name;
            Age = _age;
        }
        void sayHello()
        {
            Console.WriteLine($"Aloha!");
            Console.WriteLine($"My name is {Name} and I am {Age} years old!");
        }

        public static void Main(string[] args)
        {
            var person1 = new Person("PeppaPig", 42); //Task A - Call Method
            person1.sayHello();

            Console.WriteLine();

            var checkout1 = new Checkout(5, 2.89); //Task B Call Method
            checkout1.quantityXprice();
        }

    } 
    class Checkout 
    {
        int Quantity; //Task B - Create new Class Checkout
        double Price;

        public Checkout(int _quantity, double _price) //Constructor
        {
            Quantity = _quantity;
            Price = _price;
        }
        public void quantityXprice ()
        {
            Console.WriteLine($"{Quantity} x {Price} = {Quantity*Price}");
        }
    }
    
}
